// Soal 1
const halo = () => "Halo Sanbers!";
console.log(halo());
console.log();

// Soal 2
const kalikan = (a, b) => a * b;
const num1 = 12
const num2 = 4
const hasilKali = kalikan(num1, num2);
console.log(hasilKali);
console.log();

// Soal 3
const introduce = (name, age, address, hobby) => 
    `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
    
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 
console.log();
