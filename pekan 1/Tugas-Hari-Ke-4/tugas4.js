// SOal 1

console.log('LOOPING PERTAMA');
let i = 1;
while(i <= 20) {
    if (i % 2 === 0) {
        console.log(`${i} I love coding`);
    }
    i+=1;
} 

console.log('LOOPING KEDUA');
while(i >= 1) {
    if (i % 2 === 0) {
        console.log(`${i} I will become a frontend developer`);
    }
    i-=1;
}


// SOal 2
for(let i = 1; i <= 20; i+=1) {
    if (i % 2 === 0) {
        console.log(`${i} Berkualitas`)
    } else if (i % 3 === 0) {
        console.log(`${i} I Love Coding`);
    } else {
        console.log(`${i} Santai`);
    }
}

console.log();
// Soal 3
for (let i = 0; i < 7; i++) {
    for(let j = 0; j <= i; j++) {
        process.stdout.write('#');
    }
    process.stdout.write('\n');
}

// Soal 4
const kalimat ="saya sangat senang belajar javascript"
const newKal = kalimat.split(' ');
console.log(newKal);


// Soal 5
const daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
const daftarBuahSorted = [];
for(let i = 0; i < daftarBuah.length; i++) {
    // console.log(tmp);
    for(let j = i; j < daftarBuah.length; j++) {
        if (daftarBuah[i] > daftarBuah[j]) {
            let tmp = daftarBuah[i];
            daftarBuah[i] = daftarBuah[j];
            daftarBuah[j] = tmp;
        }
    }
}

console.log(daftarBuah);
