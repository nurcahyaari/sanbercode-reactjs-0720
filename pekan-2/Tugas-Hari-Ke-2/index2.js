var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const readBooks = async () => {
    let time = 10000;
    for (const book of books) {
        time = await readBooksPromise(time, book);
    }
    console.log(`Sisa waktu saya ${time}`);
}

readBooks();
