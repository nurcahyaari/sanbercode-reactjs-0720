import React from 'react';
import './App.css';

function App() {
  return (
    <div style={{ padding: 10, display: 'flex', justifyContent: 'center' }}>
      <div style={{ border: '1px solid black', borderRadius: '10px', width: '30%', padding: 100 }}>
        <div style={{ justifyContent: 'center', display: 'flex', marginBottom: 50 }}>
          <h1>Form Pembelian Buah</h1>
        </div>
        <div>
          <table>
            <tr>
              <td style={{ width: 150 }}>Nama</td>
              <td>
                <input type="text" />
              </td>
            </tr>
            <tr>
              <td style={{ verticalAlign: 'bottom' }}>Daftar Item</td>
              <td>
                <div style={{ display: 'block' }}>
                  <input type="checkbox"  /> Semangka
                </div>
                <div style={{ display: 'block' }}>
                  <input type="checkbox" /> Jeruk
                </div>
                <div style={{ display: 'block' }}>
                  <input type="checkbox" /> Nanas
                </div>
                <div style={{ display: 'block' }}>
                  <input type="checkbox" /> Salak
                </div>
                <div style={{ display: 'block' }}>
                  <input type="checkbox" /> Anggur
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
