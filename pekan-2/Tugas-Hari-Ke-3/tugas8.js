// Soal 1
const LuasLingkaran = (r) => ((22/7) * (r * r));
const KelilingLingkaran = (r) => (2 * (22/7) * r);

console.log(LuasLingkaran(10));
console.log(KelilingLingkaran(10));

// Soal 2
let kalimat = ``

const kal1 = 'saya';
const kal2 = 'adalah';
const kal3 = 'seorang';
const kal4 = 'frontend';
const kal5 = 'developer';

kalimat = `${kal1} ${kal2} ${kal3} ${kal4} ${kal5}`;

console.log(kalimat);

// Soal3
class Book {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }
}

class Comic extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this.isColorful = isColorful;
    }
}
