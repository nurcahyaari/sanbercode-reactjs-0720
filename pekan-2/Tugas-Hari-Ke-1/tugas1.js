// Soal 1
const arrayDaftarPeserta = {
    name: 'Asep',
    gender: 'Laki-laki',
    hobby: 'baca buku',
    born: 1992,
};
console.log(arrayDaftarPeserta);
console.log();

// Soal 2
// 1.nama: strawberry
//   warna: merah
//   ada bijinya: tidak
//   harga: 9000 
// 2.nama: jeruk
//   warna: oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000
const listBuah = [
    {
        nama: 'strawberry',
        warna: 'merah',
        'ada bijinya': 'tidak',
        harga: 9000 ,
    },
    {
        nama: 'jeruk',
        warna: 'oranye',
        'ada bijinya': 'ada',
        harga: 8000,
    },
    {
        nama: 'Semangka',
        warna: 'Hijau & Merah',
        'ada bijinya': 'ada',
        harga: 10000,
    },
    {
        nama: 'Pisang',
        warna: 'Kuning',
        'ada bijinya': 'tidak',
        harga: 5000,
    }
]

console.log(listBuah[0]);
console.log();

// Soal 3
const dataFilm = []
console.log(dataFilm);
dataFilm.push({
    nama: 'Godzilla 2014',
    durasi: '120m',
    gendre: 'Kaiju',
    tahun: 2014,
})
console.log(dataFilm);
console.log();

// Soal 4
class Animal {
    constructor(name, legs = 4) {
        this.legs = legs;
        this.cold_blooded = false;
        this.name = name;
    }
}

// Release 0
const sheep = new Animal("shaun");
 
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false
console.log();

// Release 1
class Ape extends Animal {
    constructor(name) {
        super(name, 2);
    }

    yell() {
        console.log('Auooo');
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        console.log('Hip hop');
    }
}

const sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
console.log(sungokong.name); // "shaun"
console.log(sungokong.legs); // 4
console.log(sungokong.cold_blooded); // false
 
const kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log(kodok.name); // "shaun"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // false
console.log();

// Soal 5
class Clock {
    // Code di sini
    constructor({template}) {
        this.timer;
        this.template = template;
        this.render = this.render.bind(this);
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        this.output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(this.output);
    }

    stop() {
        clearInterval(this.timer);
    };

    start() {
        this.timer = setInterval(this.render, 1000);
    }
}

const clock = new Clock({template: 'h:m:s'});
clock.start();  
